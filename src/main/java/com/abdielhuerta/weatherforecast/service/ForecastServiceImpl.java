package com.abdielhuerta.weatherforecast.service;

import com.abdielhuerta.weatherforecast.service.dto.ForecastResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class ForecastServiceImpl extends AbstractClient implements ForecastService {
    String lat;
    String lon;
    public ForecastServiceImpl(RestTemplate restTemplate){super(restTemplate);}

    @Override
    public ForecastResponse findCity(String lat, String lon, String appid){

        String uri = baseUrl + "?lat="+lat+"&lon="+lon+"&exclude=current,minutely,hourly,alerts&appid="+appid;
        HttpEntity<Void> requestEntity = new HttpEntity<>(this.buildAuthToken());
        ResponseEntity<ForecastResponse> response = restTemplate.exchange(uri, HttpMethod.GET,requestEntity,ForecastResponse.class);

        if(response.getStatusCode().is2xxSuccessful()){
            return response.getBody();
        }
        log.error("Error retrieving information - httpStatus was: {}", response.getStatusCode());
        throw new RuntimeException("Error");

    }
}
