package com.abdielhuerta.weatherforecast.service;

import com.abdielhuerta.weatherforecast.service.dto.ForecastResponse;

public interface ForecastService {
    ForecastResponse findCity(String lan, String lon, String appid);
}
