package com.abdielhuerta.weatherforecast.service.dto;

import lombok.Data;

@Data
public class City {
    private String lat;
    private String lon;
}
