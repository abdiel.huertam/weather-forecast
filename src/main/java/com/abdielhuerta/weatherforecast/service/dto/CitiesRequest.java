package com.abdielhuerta.weatherforecast.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
@Data
public class CitiesRequest {
    @JsonProperty("cities")
    private List<City> cities;
}
