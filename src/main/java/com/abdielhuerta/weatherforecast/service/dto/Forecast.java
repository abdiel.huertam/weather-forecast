package com.abdielhuerta.weatherforecast.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Forecast {
    private String dt;
    private String sunrise;
    private String sunset;
    private String moonrise;
    private String moonset;
    private String moon_phase;
    //private String temp;
    //private String feels_like;
    private String pressure;
    private String humidity;
    private String dew_point;
    private String wind_speed;
    private String wind_deg;
    //private String weather;
    private String clouds;
    private String pop;
    private String rain;
    private String uvi;
}
