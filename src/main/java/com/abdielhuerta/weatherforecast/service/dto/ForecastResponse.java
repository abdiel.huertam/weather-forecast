package com.abdielhuerta.weatherforecast.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
@Data
public class ForecastResponse {
    public String lat;
    public String lon;
    public String timezone;
    public String timezone_offset;
    @JsonProperty("daily")
    public List<Forecast> daily;
}
