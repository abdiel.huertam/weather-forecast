package com.abdielhuerta.weatherforecast.controller;

import com.abdielhuerta.weatherforecast.service.ForecastService;
import com.abdielhuerta.weatherforecast.service.dto.CitiesRequest;
import com.abdielhuerta.weatherforecast.service.dto.ForecastResponse;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("weather/forecast/appid={appid}")
public class ForecastController {
    private final ForecastService forecastService;

    public ForecastController(ForecastService forecastService){this.forecastService = forecastService;}

    @GetMapping
    public ArrayList findAll(@RequestBody CitiesRequest cities, @PathVariable String appid){
        String lat="";
        String lon="";
        List<ForecastResponse> citiesResponseList= new ArrayList<>();
        for (int i=0;i<cities.getCities().size();i++){
            lat = cities.getCities().get(i).getLat().toString();
            lon = cities.getCities().get(i).getLon().toString();
            citiesResponseList.add(this.forecastService.findCity(lat,lon,appid));
        }
        return (ArrayList) citiesResponseList;
    }

}
